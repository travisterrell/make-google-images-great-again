# Make Google Images Great Again

Brings the [View Image] and [Search by Image] buttons back to Google Images search results. Google removed this functionality as a concession in their negotiations with Getty Images. See [this article](https://searchengineland.com/google-image-search-removes-view-image-button-search-image-feature-292183).

## Getting Started

Install for Firefox: https://addons.mozilla.org/en-US/firefox/addon/make-google-images-great-again/

Install for Chrome:
https://chrome.google.com/webstore/detail/make-google-images-great/kjlaimldplcbblpgenlhfjpobmolcblj

### Notes and Caveats

This is a pretty simple, and not particulary sophisticated reverse engineering of the Google Images search results page DOM. It is unclear whether it will work on non-english versions of Google, and how often they might change the DOM of that page sufficiently to break this.

Pull requests are more than welcome.

As are beers: https://paypal.me/donnieclapp

## Built With

* [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) - A command line tool for packaging browser extensions from Mozilla
* [javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) - Just plain old javascript

## Contributing

Contributing is good. Please do. Submit a pull request at https://gitlab.com/donnieclapp-OS/make-google-images-great-again

## Authors

* **Donnie Clapp** - *Initial work* 

## License

This project is licensed under the GNU Lesser Public License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to [Clients From Hell](https://twitter.com/clientsfh) for tweeting about Google's decision to remove these buttons. See the thread here - https://twitter.com/clientsfh/status/964248436307406848.

